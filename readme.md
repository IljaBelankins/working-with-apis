# Installation

To install:
```
pip install -r requirements.txt
```

# Run

To run:
```
python api.py
```