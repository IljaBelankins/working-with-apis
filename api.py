from flask import Flask, render_template
from github_interface import get_github_repos

app = Flask(__name__)

@app.route("/")
def graph():

    repos = get_github_repos()
    stars_list = []
    names_list = []
    for item in repos:
        names_list.append("'" + item['name'] + "'")
        stars_list.append(str(item['stars']))


    names_string = ', '.join(names_list)
    stars_string = ', '.join(stars_list)
    return render_template("visualisation.html", names_list = names_string, stars_list = stars_string)

        

    


if __name__ == '__main__':
    app.run()