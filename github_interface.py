import requests

def get_github_repos():
    url = 'https://api.github.com/search/repositories?q=language:python&sort=stars'
    headers = {'Accept': 'application/vnd.github.v3+json'}
    r = requests.get(url, headers=headers)
    response_dict = r.json()
    repo_dicts = response_dict['items'][0:5]
    final_repo_dicts = []
    for item in repo_dicts:
        final_repo_dicts.append(
            {'name': item['name'],
            'stars': item['stargazers_count']}
        )
    return final_repo_dicts



    